-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 03-10-2015 a las 20:52:34
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cms_autogestion`
--
CREATE DATABASE `cms_autogestion` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cms_autogestion`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

CREATE TABLE IF NOT EXISTS `archivos` (
  `TSid` bigint(20) NOT NULL AUTO_INCREMENT,
  `TSDNI` bigint(20) NOT NULL,
  `TSNombreArchivo` varchar(250) NOT NULL,
  `TSTipo` varchar(250) NOT NULL,
  `TSDetalle` varchar(250) NOT NULL,
  `TSFecha` date NOT NULL,
  UNIQUE KEY `TSid` (`TSid`),
  KEY `TSDNI` (`TSDNI`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadocuenta`
--

CREATE TABLE IF NOT EXISTS `estadocuenta` (
  `VECid` bigint(20) NOT NULL AUTO_INCREMENT,
  `VECorden` char(5) DEFAULT NULL,
  `VECIDLiq` bigint(20) DEFAULT NULL,
  `VECConcepto` char(100) DEFAULT NULL,
  `VECDetalle` char(100) DEFAULT NULL,
  `VECCodSocio` int(11) DEFAULT NULL,
  `VECFecha` date DEFAULT NULL,
  `VECDebito` decimal(18,2) DEFAULT NULL,
  `VECCredito` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`VECid`),
  UNIQUE KEY `VECid` (`VECid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=756 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `olvidopass`
--

CREATE TABLE IF NOT EXISTS `olvidopass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TSDNI` bigint(20) NOT NULL,
  `TSToken` varchar(250) NOT NULL,
  `TSVigencia` char(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE IF NOT EXISTS `socios` (
  `TSCodSocio` int(11) NOT NULL AUTO_INCREMENT,
  `TSDNI` bigint(20) NOT NULL,
  `TSApenom` varchar(50) NOT NULL,
  `TSMatricula` int(11) NOT NULL,
  `TSFechaNac` date NOT NULL,
  `TSDomicProf` varchar(50) NOT NULL,
  `TSCPProf` char(10) NOT NULL,
  `TSLocalProf` varchar(50) NOT NULL,
  `TSDomicPart` varchar(50) NOT NULL,
  `TSCPPart` char(10) NOT NULL,
  `TSLocalPart` varchar(50) NOT NULL,
  `TSTelefonoProf` varchar(25) NOT NULL,
  `TSTelefonoFaX` varchar(15) DEFAULT NULL,
  `TSTelefonoPart` varchar(25) NOT NULL,
  `TSTelefonoMovil` int(25) DEFAULT NULL,
  `TSMail` varchar(50) DEFAULT NULL,
  `TSFechaIngreso` date NOT NULL,
  `TSFechaEgreso` date NOT NULL,
  `TSEspecialidad1` int(11) NOT NULL,
  `TSEspecialidad12` int(11) NOT NULL,
  `TSEspecialidad13` int(11) NOT NULL,
  `TSCuit` varchar(13) NOT NULL,
  `TSHorario` varchar(160) NOT NULL,
  `TSFoto` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`TSCodSocio`),
  UNIQUE KEY `TSCodSocio` (`TSCodSocio`),
  KEY `TSDNI` (`TSDNI`,`TSMatricula`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `TSDNI` bigint(20) NOT NULL,
  `TSPrimerAcceso` char(1) NOT NULL,
  `TSContrasenia` varchar(255) NOT NULL,
  UNIQUE KEY `TSDNI` (`TSDNI`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
