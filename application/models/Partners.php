<?php
/**
* 
*/
class Partners extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function login($TSDNI, $TSContrasenia){
		$this->db->where('TSDNI', $TSDNI);
		$this->db->where('TSContrasenia', $TSContrasenia);
		$query = $this->db->get('usuarios');
		if($query->num_rows() == 1)
		{
			return $query->row();
		}else{
			return false;
		}
	}

	public function activeAccount($TSDNI, $TSContrasenia)
	{
		$data = array(
			'TSPrimerAcceso' => 'N',
        	'TSContrasenia' => $TSContrasenia
	      );
		$this->db->where('TSDNI', $TSDNI);
		$this->db->update('usuarios', $data);

		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function getPartner($TSDNI){
		$this->db->where('TSDNI', $TSDNI);
		$query = $this->db->get('socios');
		if($query->num_rows() == 1)
		{
			return $query->row();
		}else{
			return false;
		}
	}

	public function forgotPass($TSDNI, $TSMail){
		$this->db->where('TSDNI', $TSDNI);
		$this->db->where('TSMail', $TSMail);
		$query = $this->db->get('socios');
		if($query->num_rows() == 1)
		{
			$token = md5(uniqid(rand(),true));
			$data = array(
				'TSDNI' => $TSDNI,
	        	'TSToken' => $token,
	        	'TSVigencia' => 'S'
		      );
			$this->db->insert('olvidopass', $data);
			if($this->db->affected_rows() > 0){
	 			return $token;
	 		}else{
	 			return false;
	 		}
		}else{
			return false;
		}
	}

	public function checkToken($TSToken){
		$this->db->where('TSToken', $TSToken);
		$this->db->where('TSVigencia', 'S');
		$query = $this->db->get('olvidopass');
		if($query->num_rows() == 1){
			return true;
		}else
		{
			return false;
		}
	}

	public function updatePassByToken($TSToken, $TSContrasenia){
		$this->db->select('TSDNI');
		$this->db->where('TSToken', $TSToken);
		$this->db->where('TSVigencia', 'S');
		$query = $this->db->get('olvidopass');

		if($query->num_rows() == 1){
			if($this->updateStateToken($TSToken)){
				if($this->activeAccount($query->row()->TSDNI, $TSContrasenia)){
					if($this->login($query->row()->TSDNI, $TSContrasenia))
					{
						return $this->getPartner($query->row()->TSDNI);
					}else{
						return false;
					}
				}else
				{
					return false;
				}
			}
			else{
				return false;
			}
		}else
		{
			return false;
		}	
	}

	private function updateStateToken($TSToken){
		$data = array(
        	'TSVigencia' => 'N'
	      );
		$this->db->where('TSToken', $TSToken);
		$this->db->update('olvidopass', $data);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function validatePassHistoryByToken($TSToken, $TSContrasenia){
		$this->db->select('TSDNI');
		$this->db->where('TSToken', $TSToken);
		$this->db->where('TSVigencia', 'S');
		$query = $this->db->get('olvidopass');
		if($query->num_rows() == 1){
			return $this->validatePassHistory($query->row()->TSDNI, $TSContrasenia);
		}else{
			return true;
		}
	}	

	public function validatePassHistory($TSDNI, $TSContrasenia){
		$this->db->where('TSDNI', $TSDNI);
		$this->db->where('TSContrasenia', $TSContrasenia);
		$query = $this->db->get('usuarios');
		if($query->num_rows() > 0){
			return false;
		}else{
			return true;
		}
	}
}
?>