<?php
/**
* 
*/
class AccountStatement extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getAccountStatemets($TSCodSocio){
		$this->db->where('VECCodSocio', $TSCodSocio);
		$this->db->order_by('VECFecha desc,  CAST(VECorden AS SIGNED)'); 
		$query = $this->db->get('estadocuenta');
		return $query->result_array();
	}
}
?>