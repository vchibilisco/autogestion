<?php
/**
* 
*/
class Files extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getFiles($TSDNI, $filter){
		
		$this->db->where('TSDNI', $TSDNI);
		$this->db->like('TSTipo', $filter);
		$this->db->order_by('TSFecha desc'); 
		$query = $this->db->get('archivos');
		return $query->result_array();
	}
}
?>