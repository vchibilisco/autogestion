<!-- Page Content -->
<div class="container">

<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Descarga de archivos</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('home/index');?>">Home</a>
            </li>
            <li class="active">Descarga de archivos</li>
        </ol>
    </div>
</div>

<div class="center-block">
	
	<?php if($error <> ''){ ?>
		<div class="alert alert-danger" role="alert"> <?php echo $error; ?> </div>
	<?php }?>
	<table id="tableDownload" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Fecha</th>
				<th>Tipo</th>
				<th>Detalle</th>
				<th>Nombre de Archivo</th>
				<th>Accion</th>
			</tr>
		</thead>
		<tbody>
		  <!-- Aplicadas en las filas -->

		  <?php 
		  	foreach ($filesArray as $object) {
		  		?>
		  	<tr class="active">
		  		<td><?php echo $object['TSFecha']; ?></td>
		  		<td><?php echo $object['TSTipo']; ?></td>
		  		<td><?php echo $object['TSDetalle']; ?></td>
		  		<td><?php echo $object['TSNombreArchivo']; ?></td>
		  		<td style="width: 100px;"><a href="<?php echo site_url('DownloadFile/download').'/'.$object["TSNombreArchivo"];?>" class="btn btn-warning btn-sm">Descargar</a></td>
		  	</tr>
		  	<?php }
		  ?>
		  <!-- Aplicadas en las celdas (<td> o <th>) -->
		  
		</tbody>
	</table>
</div>	