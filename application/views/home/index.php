    
<!-- Header Carousel -->
<header id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style="background-image:url('<?php echo base_url(); ?>assets/images/1_1037_389.jpg');"></div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('<?php echo base_url(); ?>assets/images/ser2.jpg');"></div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('<?php echo base_url(); ?>assets/images/LogoCMS.jpg');"></div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>
<!-- Page Content -->
<div class="container"> 
    <!-- Marketing Icons Section -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Bienvenido <?php echo $this->session->userdata('partner')->TSApenom; ?> 
            </h1>
        </div>
            