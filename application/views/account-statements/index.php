<!-- Page Content -->
<div class="container">

<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Estados de Cuentas</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('home/index');?>">Home</a>
            </li>
            <li class="active">Estados de Cuentas</li>
        </ol>
    </div>
</div>

<!-- /.row -->
<div class="center-block">
	
	<table id="tableAccountStatements" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Fecha</th>
				<th>Liquidacion</th>
				<th>Detalle</th>
				<th>Debito</th>
				<th>Credito</th>
				<th>Saldo</th>
			</tr>
		</thead>
		<tbody>
		  <!-- Aplicadas en las filas -->
		  <?php 
		  	$lastVEC = 0;
		  	foreach ($accountStatementArray as $object) {
			  	if($object['VECorden'] == 1 && $lastVEC == 9999){
			  		echo '<tr style="background-color: #ADE2C9 !important">';
			  	}else{
			  		echo '<tr>';
			  	}
			  		echo '<td>'.$object['VECFecha'].'</td>';
			  		echo '<td>'.$object['VECIDLiq'].'</td>';
			  		echo '<td>'.$object['VECDetalle'].'</td>';
			  		echo '<td>'.$object['VECDebito'].'</td>';
			  		echo '<td>'.$object['VECCredito'].'</td>';
			  		if($object['VECSaldo'] < 0){
			  			echo '<td style="background-color: #F36767;">'.$object['VECSaldo'].'</td>';
			  		}else{
			  			echo '<td>'.$object['VECSaldo'].'</td>';
			  		}
			  	echo '</tr>';
			  	$lastVEC = $object['VECorden'];
		  	}
		  ?>
		  <!-- Aplicadas en las celdas (<td> o <th>) -->
		  
		</tbody>
	</table>
</div>