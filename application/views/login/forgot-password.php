<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  <h1 class="text-center">Reestablecer su contraseña</h1>
</div>
<div class="modal-body">
  <?php
      
    $attributes = array('class' => 'form col-md-12 center-block');
    echo form_open('login/forgotPassword/', $attributes);     
    if( isset($err_message)){
      echo '<div class="alert alert-danger" role="alert">';
      echo $err_message;
      echo '</div>';
    }
    echo '<div class="form-group">';
    $data = array(
      'name' => 'dni',
      'type' => 'text',
      'class' => 'form-control input-lg',
      'placeholder' => 'Ingrese su DNI'
      );
    echo form_input($data);
    echo '</div>';
    echo '<div class="form-group">';
    $data = array(
      'name' => 'email',
      'type' => 'email',
      'class' => 'form-control input-lg',
      'placeholder' => 'Ingrese su email'
      );
    echo form_input($data);
    echo '</div>';
    echo '<div class="form-group">';
    echo form_submit('submit', 'Enviar', "class='btn btn-primary btn-lg btn-block'");
    echo '</div>';

  ?>
</div>
<div class="modal-footer">
  	
</div>