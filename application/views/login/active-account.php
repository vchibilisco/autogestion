<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  <h1 class="text-center">Re-ingrese sus datos</h1>
</div>
<div class="modal-body">
  <?php
      
    $attributes = array('class' => 'form col-md-12 center-block');
    echo form_open('login/activeaccount/'.$id, $attributes);     
    if( isset($err_message)){
      echo '<div class="alert alert-danger" role="alert">';
      echo $err_message;
      echo '</div>';
    }
    echo validation_errors('<div class="alert alert-danger" role="alert">', '</div>');
    echo '<div class="form-group">';
    $data_pass = array(
      'name' => 'password',
      'type' => 'password',
      'class' => 'form-control input-lg',
      'placeholder' => 'Nueva Contraseña'
      );
    echo form_input($data_pass);
    echo '</div>';
    echo '<div class="form-group">';
    $data_pass2 = array(
      'name' => 'password2',
      'type' => 'password',
      'class' => 'form-control input-lg',
      'placeholder' => 'Re-ingrese Nueva Contraseña'
      );
    echo form_password($data_pass2);
    echo '</div>';
    echo '<div class="form-group">';
    echo form_submit('submit', 'Activar Cuenta', "class='btn btn-primary btn-lg btn-block'");
    echo '</div>';

  ?>
</div>
<div class="modal-footer">
  	
</div>
