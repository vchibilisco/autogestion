<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  <h1 class="text-center">Bienvenido</h1>
</div>
<div class="modal-body">
  <?php
    
    $attributes = array('class' => 'form col-md-12 center-block');
    echo form_open('login/index',$attributes);
    if( isset($err_message)){
      echo '<div class="alert alert-danger" role="alert">';
      echo $err_message;
      echo '</div>';
    }
    echo validation_errors('<div class="alert alert-danger" role="alert">', '</div>');
    echo '<div class="form-group">';
    $data_email = array(
      'name' => 'dni',
      'class' => 'form-control input-lg',
      'placeholder' => 'DNI'
      );
    echo form_input($data_email);
    echo '</div>';
    echo '<div class="form-group">';
    $data_email = array(
      'name' => 'password',
      'class' => 'form-control input-lg',
      'placeholder' => 'Contraseña'
      );
    echo form_password($data_email);
    echo '</div>';
    echo '<div class="form-group">';
    echo form_submit('submit', 'Iniciar Sesion', "class='btn btn-primary btn-lg btn-block'");
    echo '</div>';

  ?>
</div>
<div class="modal-footer">
  	<a href="<?php echo site_url('login/forgotPassword');?>">Olvidó su contraseña?</a>
</div>