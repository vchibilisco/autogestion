<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DownloadFile extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('download');
		$this->load->model('Files');
	}

	function index($error = '', $filter = 'all')
	{
		$this->isLogin();
		$partner = $this->session->userdata('partner');

		switch ($filter) {
			case 'all':
				$filter = '';
				break;
			case 'recibos':
				$filter = 'Recibo';
				break;
			case 'detalles':
				$filter = 'Detalle';
				break;
		}

		$filesArray = $this->Files->getFiles($partner->TSDNI, $filter);
		$data['webTitle'] = "Auto-gestion";
		$data['filesArray'] = $filesArray;
		$data['error'] = $error;
		$this->load->view('/templates/web/header', $data);
		$this->load->view('/download-file/index', $data);
		$this->load->view('/templates/web/footer');
	}

	public function download($fileName)
	{
		if(file_exists(FCPATH."resource/files/".$fileName)){
			$data = file_get_contents(FCPATH."resource/files/".$fileName);
			force_download($fileName, $data);	
		}
		$error = 'No se encuentra el archivo seleccionado.';
		$this->index($error);
	}
}
