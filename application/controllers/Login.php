<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('form');

		$this->load->library('form_validation');
		$this->load->library('encrypt');

		$this->load->model('Partners');
	}

	public function index()
	{
		$this->load->helper('security');
		$this->form_validation->set_rules('dni', 'DNI', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE) {
			$data["pageTitle"] = 'Iniciar Sesion';
			$this->loadLogin('index', $data);
		}else{
			$dni = $this->input->post('dni');
			$pass = md5($this->input->post('password'));

			$partner = $this->Partners->login($dni, $pass);
			
			if($partner){
				if($partner->TSPrimerAcceso == 'S' ){
					redirect(base_url().'login/activeaccount/'.$partner->TSDNI,'refresh');
				}else{
					$partner = $this->Partners->getPartner($partner->TSDNI);
					$exists = FCPATH."resource/picturesPartners/".$partner->TSFoto;
					if(file_exists($exists)){
						$partner->TSFoto = base_url()."resource/picturesPartners/".$partner->TSFoto;
					}else{
						$partner->TSFoto = false;
					}
					$this->setSession($partner);
					redirect(base_url().'home','refresh');
				}
			}else{
				$data['err_message'] = 'El socio no existe';
				$data["pageTitle"] = 'Iniciar Sesion';
				$this->loadLogin('index', $data);
			}
			
		}
	}

	public function activeAccount($id){
		$this->load->helper('security');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]|xss_clean');
		$this->form_validation->set_rules('password2', 'Re-Password', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE) {
			$data["pageTitle"] = 'Activar Cuenta';
			$data["id"] = $id;
			$this->loadLogin('active-account', $data);
		}else{
			$pass = md5($this->input->post('password'));
			
			if($this->validatePassHistory($id, $pass)){
				if($this->Partners->activeAccount($id, $pass)){
					$partner = $this->Partners->getPartner($id);
					redirect(base_url().'home','refresh');	
				}
			}
			else{
				$data['err_message'] = 'La contraseña debe ser distinta a la actual';
				$data["pageTitle"] = 'Activar Cuenta';
				$data["id"] = $id;
				$this->loadLogin('active-account', $data);
			}
		}
	}

	private function loadLogin($page, $data)
	{
		$this->load->view('/templates/login/header',$data);
		$this->load->view('/login/'.$page, $data);		
		$this->load->view('/templates/login/footer');
	}

	private function setSession($partner){
		$data = array(
			'isLogin' => TRUE,
			'partner' => $partner
			);
		$this->session->set_userdata($data);
	}

	public function logout(){
		$this->session->unset_userdata('isLogin');
		$this->session->unset_userdata('partner');
		$data["pageTitle"] = 'Iniciar Sesion';
		$this->loadLogin('index', $data);
	}

	public function forgotPassword(){
		$this->load->helper('security');
		$this->form_validation->set_rules('dni', 'DNI', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
		
		if ($this->form_validation->run() == FALSE) {
			$data["pageTitle"] = 'Reestablecer Contraseña';
			$this->loadLogin('forgot-password', $data);
		}else{
			$dni = $this->input->post('dni');
			$email = $this->input->post('email');
			
			$token = $this->Partners->forgotPass($dni, $email);

			if($token != false){
				if( ! ini_get('date.timezone') )
				{
				   date_default_timezone_set('GMT');
				}
				// Storing submitted values
				$sender_email = 'soporte@circulomedicodelsur.org.ar';
				$user_password = 'Sycit2015';
				$receiver_email = $email;
				$subject = 'Reestablecer Contraseña';
				$message = '';

				// Configure email library
				$config['protocol'] = 'mail';
				$config['smtp_host'] = 'mail.circulomedicodelsur.org.ar';
				$config['smtp_port'] = 2525;
				$config['mailtype'] = 'html';
				$config['smtp_user'] = $sender_email;
				$config['smtp_pass'] = $user_password;

				// Load email library and passing configured values to email library
				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");

				// Sender email address
				$this->email->from($sender_email);
				// Receiver email address
				$this->email->to($receiver_email);
				$this->email->bcc($sender_email);
				// Subject of email
				$this->email->subject($subject);

				$message = '<p>Hacer click en el siguiente enlace para reestablecer su contraseña.</p>';
				$message .= '<p>'.base_url().'login/resetPassword/'.$token.'</p>';

				$this->email->message($message);
				if (!$this->email->send()) {
					$data['err_message'] = 'Ocurrio un error, no se envio el email.';
				} 
				$data["pageTitle"] = 'Iniciar Sesion';
				$this->loadLogin('index', $data);
			}else{
				$data['err_message'] = 'Ocurrio un error';
				$data["pageTitle"] = 'Reestablecer Contraseña';
				$this->loadLogin('forgot-password', $data);
			}
			
		}
	}

	public function resetPassword($token = ''){
		if($this->Partners->checkToken($token)){
			$this->load->helper('security');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]|xss_clean');
			$this->form_validation->set_rules('password2', 'Re-Password', 'trim|required|xss_clean');
			
			if ($this->form_validation->run() == FALSE) {
				$data["pageTitle"] = 'Reestablecer Contraseña';
				$data["token"] = $token;
				$this->loadLogin('reset-password', $data);
			}else{
				$pass = md5($this->input->post('password'));
				
				if($this->Partners->validatePassHistoryByToken($token, $pass)){
					$partner = $this->Partners->updatePassByToken($token, $pass);
					if($partner != false){
						$this->setSession($partner);
						redirect(base_url().'home','refresh');
					}else{
						$data['err_message'] = 'Ocurrio un error.';
						$data["pageTitle"] = 'Reestablecer Contraseña';
						$data["token"] = $token;
						$this->loadLogin('reset-password', $data);
					}
				}else{
					$data['err_message'] = 'La contraseña debe ser distinta a la actual';
					$data["pageTitle"] = 'Reestablecer Contraseña';
					$data["token"] = $token;
					$this->loadLogin('reset-password', $data);
				}
			}
		}
		else{
			$data['err_message'] = 'No se reconoce el token.';
			$data["pageTitle"] = 'Iniciar Sesion';
			$this->loadLogin('index', $data);
		}
	}

	private function validatePassHistory($id, $pass){
		return $this->Partners->validatePassHistory($id, $pass);
	}
}
