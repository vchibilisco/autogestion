<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccountStatements extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('AccountStatement');
	}

	public function index()
	{
		$this->isLogin();
		$partner = $this->session->userdata('partner'); 

		$accountStatement = $this->AccountStatement->getAccountStatemets($partner->TSCodSocio);

		for($i = 0; $i < sizeof($accountStatement); $i++){
			if($i == 0){
				$accountStatement[$i]['VECSaldo'] = bcsub($this->isNull($accountStatement[$i]['VECCredito']), $this->isNull($accountStatement[$i]['VECDebito']), 2);
			}else{
				 $partial = bcadd ( $this->isNull($accountStatement[$i-1]['VECSaldo']), $this->isNull($accountStatement[$i]['VECCredito']) , 2);
				$accountStatement[$i]['VECSaldo'] = bcsub($partial, $this->isNull($accountStatement[$i]['VECDebito']), 2);
			}

		}
		
		$data['webTitle'] = "Auto-gestion";
		$data['accountStatementArray'] = $accountStatement;
		$this->load->view('/templates/web/header', $data);
		$this->load->view('/account-statements/index', $data);
		$this->load->view('/templates/web/footer');
	}

	private function isNull($value){
		if(is_null($value)){
			return 0.00;
		}else
		{
			return $value;
		}
	}
}
