<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	function index()
	{
		$this->isLogin();
		$data['webTitle'] = "Auto-gestion";
		$this->load->view('/templates/web/header', $data);
		$this->load->view('/home/index');
		$this->load->view('/templates/web/footer');
	}
}
