$(document).ready(function() {
    $('#tableAccountStatements').DataTable( {
        "pagingType": "full_numbers",
        "searching": false,
        "ordering": false,
        "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "info": "Mostrar _PAGE_ página de _PAGES_",
          "paginate": {
            "first": "Primera página",
            "previous": "Anterior",
            "next": "Siguiente",
            "last": "Última página"
          }
        }
      } 
    );
  }
);

$(document).ready(function() {
    $('#tableDownload').DataTable( {
        "pagingType": "full_numbers",
        "ordering": false,
        "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "info": "Mostrar _PAGE_ página de _PAGES_",
          "search": "Buscar:",
          "paginate": {
            "first": "Primera página",
            "previous": "Anterior",
            "next": "Siguiente",
            "last": "Última página"
          }
        }
      } 
    );
  }
);